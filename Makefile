CFLAGS = -Wall -std=c11 -I/usr/local/include
LDLIBS = -lpng16 -lraw -L/usr/local/lib
REMOVEFILES = *.png example RAWtoPNG_simple

simple : RAWtoPNG_simple
	./RAWtoPNG_simple

example : example

clean :
	rm $(REMOVEFILES)
