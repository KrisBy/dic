
# README 


# Branching #

2 Branches from the master have been created.

* Casper-og-Jonas is the branch name for Casper and Jonas
* Kristoffer-og-Emil is the branch name for Kristoffer og Emil


To get these branches locally use:

* git fetch


To check-out a branch use:

* git checkout branchname


Now you are on the new branch and you can work as before but now instead of using "origin master" use "origin branchname"

Remember to still pull with rebase before pushing to a branch

# libraw #
* Install libraw from https://www.libraw.org/download (LibRaw-0.18.5)
* Install as described in INSTALL file
* A problem which occured was that the library was not found correctly "./RAWtoPNG_simple: error while loading shared libraries: libraw.so.16: cannot open shared object file: No such file or directory"
* * echo $LD_LIBRARY_PATH returns an empty string. 
* * Fix: (run the following) 
* * * LD_LIBRARY_PAT=/usr/local/lib  
* * * export LD_LIBRARY_PATH


# libpng #
* We use "libpng-1.6.32.tar.xz" from https://sourceforge.net/projects/libpng/files/
* To fix a library issue after installation "sudo ldconfig"

# git workflow #
* Before pushing changes: "git pull --rebase origin master" to retrieve the current master on bitbucket locally. Your current commits will be placed afterwards in the tree locally.
* Now you can push using "git push origin master"
* Locally you add files ("git add <some-file>") and commit them to your local repository (git commit -m "Add/Change/Remove (what did you do)")
 
* This will change if we use branches.

### What is this repository for? ###


The purpose of this repository is to develop a C code for the Delphini onboard image compression. 
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact